""" This module handles world clock related utility. """

from discord.ext import commands
from datetime import datetime
from pytz import country_names, country_timezones, timezone
from pagermaid import des_handler, par_handler
from pagermaid.utils import process_command


class Time(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def clock(self, context):
        """ For querying time. """
        message = process_command(context)
        if len(message.parameters) > 1:
            await context.reply("出错了呜呜呜 ~ 无效的参数。")
            return
        if len(message.parameters) == 1:
            country = message.arguments.title()
        else:
            country = 'China'
        time_form = "%I:%M %p"
        date_form = "%A %d/%m/%y"
        if not country:
            time_zone = await get_timezone('China')
            await context.reply(
                f"**北京时间**\n"
                f"`{datetime.now(time_zone).strftime(date_form)} "
                f"{datetime.now(time_zone).strftime(time_form)}`"
            )
            return

        time_zone = await get_timezone(country)
        if not time_zone:
            await context.reply("出错了呜呜呜 ~ 无效的参数。")
            return

        try:
            country_name = country_names[country]
        except KeyError:
            country_name = country

        await context.reply(f"**{country_name} 时间：**\n"
                            f"`{datetime.now(time_zone).strftime(date_form)} "
                            f"{datetime.now(time_zone).strftime(time_form)}`")


async def get_timezone(target):
    """ Returns timezone of the parameter in command. """
    if "(Uk)" in target:
        target = target.replace("Uk", "UK")
    if "(Us)" in target:
        target = target.replace("Us", "US")
    if " Of " in target:
        target = target.replace(" Of ", " of ")
    if "(Western)" in target:
        target = target.replace("(Western)", "(western)")
    if "Minor Outlying Islands" in target:
        target = target.replace("Minor Outlying Islands", "minor outlying islands")
    if "Nl" in target:
        target = target.replace("Nl", "NL")

    for country_code in country_names:
        if target == country_names[country_code]:
            return timezone(country_timezones[country_code][0])
    try:
        if country_names[target]:
            return timezone(country_timezones[target][0])
    except KeyError:
        return


des_handler('clock', '显示特定区域的时间，如果参数为空，则默认显示中国。')
par_handler('clock', '<地区>')


def setup(bot):
    bot.add_cog(Time(bot))
