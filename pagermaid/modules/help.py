""" The help module. """

import discord
from discord.ext import commands
from pagermaid import color, prefix, des_handler, par_handler, des_map, par_map
from pagermaid.utils import process_command


class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def help(self, context):
        message = process_command(context)
        if len(message.parameters) == 0:
            embed = discord.Embed(title="帮助菜单", description="以下是 bot 支持的命令列表", color=color)
            for com, des in des_map.items():
                embed.add_field(name=f"{prefix}{com} {par_map[com]}", value=f"{des}", inline=True)
            await context.reply(embed=embed)
        else:
            if message.arguments in des_map:
                com = message.arguments
                embed = discord.Embed(title="使用帮助", description=f"命令 {com} 帮助", color=color)
                embed.add_field(name=f"{prefix}{com} {par_map[com]}", value=f"{des_map[com]}", inline=True)
                await context.reply(embed=embed)
            else:
                await context.reply('您好像输入了一个无效的参数。')


des_handler('help', '显示所有命令。')
par_handler('help', '<command>')


def setup(bot):
    bot.add_cog(Help(bot))
