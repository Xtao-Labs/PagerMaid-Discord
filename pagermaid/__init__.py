""" PagerMaid initialization. """

import discord
from typing import Dict
from subprocess import run, PIPE
from discord.ext import commands
from os import getcwd
from distutils.util import strtobool
from yaml import load, FullLoader
from redis import StrictRedis
from coloredlogs import ColoredFormatter
from logging import getLogger, INFO, DEBUG, ERROR, StreamHandler, basicConfig

persistent_vars = {}
module_dir = __path__[0]
working_dir = getcwd()
config = None
des_map: Dict[str, str] = {}
par_map: Dict[str, str] = {}
logs = getLogger(__name__)
logging_format = "%(levelname)s [%(asctime)s] [%(name)s] %(message)s"
logging_handler = StreamHandler()
logging_handler.setFormatter(ColoredFormatter(logging_format))
root_logger = getLogger()
root_logger.setLevel(ERROR)
root_logger.addHandler(logging_handler)
basicConfig(level=INFO)
logs.setLevel(INFO)

try:
    config = load(open(r"config.yml"), Loader=FullLoader)
except FileNotFoundError:
    logs.fatal("错误：配置文件不存在。")
    exit(1)

if strtobool(config['debug']):
    logs.setLevel(DEBUG)
else:
    logs.setLevel(INFO)

token = config['token']
color = int(config['color'], 16)
prefix = config['prefix']
game_des = config['game_des']
redis_host = config['redis']['host']
redis_port = config['redis']['port']
redis_db = config['redis']['db']
redis = StrictRedis(host=redis_host, port=redis_port, db=redis_db)
git_hash = run("git rev-parse HEAD", stdout=PIPE, shell=True).stdout.decode()
if token == 'Here':
    logs.fatal("错误：Token 不存在。")
    exit(1)
if game_des:
    bot = commands.Bot(
        command_prefix=prefix,
        activity=discord.Game(
            name=game_des
        )
    )
else:
    bot = commands.Bot(command_prefix=prefix)
bot.remove_command('help')


def des_handler(cmd: str, des: str) -> None:
    global des_map
    if des_map.get(cmd) is not None:
        return
    des_map[cmd] = des


def par_handler(cmd: str, par: str) -> None:
    global par_map
    if par_map.get(cmd) is not None:
        return
    par_map[cmd] = par


def redis_status():
    try:
        redis.ping()
        return True
    except BaseException:
        return False


async def log(message):
    logs.info(
        message.replace('`', '\"')
    )
    if not strtobool(config['log']):
        return
