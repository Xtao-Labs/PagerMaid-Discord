""" PagerMaid launch sequence. """

from sys import path
from pagermaid import bot, logs, working_dir, token
from pagermaid.modules import module_list, plugin_list


path.insert(1, f"{working_dir}/plugins")
for module_name in module_list:
    try:
        bot.load_extension("pagermaid.modules." + module_name)
    except BaseException as exception:
        logs.info(f"模块 {module_name} 加载出错: {type(exception)}: {exception}")
for plugin_name in plugin_list:
    try:
        bot.load_extension("plugins." + plugin_name)
    except BaseException as exception:
        logs.info(f"插件 {plugin_name} 加载出错: {exception}")
        plugin_list.remove(plugin_name)
logs.info('PagerMaid-Discord 已启动，在任何频道中输入 /help 以获得帮助消息。')
bot.run(token)
